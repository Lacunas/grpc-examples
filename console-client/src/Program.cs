﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Google.Protobuf;
using Grpc.Net.Client;
using GrpcExample.Core.Models.Proto.Api;
using GrpcExample.Core.Models.Proto.StorageProviders;
using Xunit;

namespace console_client
{
    class Program
    {
        private static X509Certificate2 GetCertificate()
        {
            const string path = "../content/Alan Mathison Turing.pfx";
            return new X509Certificate2(path, "1234", X509KeyStorageFlags.MachineKeySet);
        }

        public static byte[] GetCertPublicContent()
        {
            var certificate = GetCertificate();
            return certificate.Export(X509ContentType.Cert);
        }

        public static byte[] SignHash(X509Certificate2 certificate, byte[] toSignHash)
        {
            var key = certificate.GetRSAPrivateKey();
            return key.SignHash(toSignHash, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
        }

        static async Task Main()
        {
            var pdf = await File.ReadAllBytesAsync("../content/SampleDocument.pdf");
            // var pdf = await File.ReadAllBytesAsync("../content/vcCu2P.pdf");

            var request = new SignatureStartRequest
            {
                Certificate = ByteString.CopyFrom(GetCertPublicContent()),
                Content = new ProvidedOnRequest { Raw = ByteString.CopyFrom(pdf) },
            };

            var httpClientHandler = new HttpClientHandler
            {
                // Return `true` to allow certificates that are untrusted/invalid
                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator,
                AllowAutoRedirect = true,
            };

            var httpClient = new HttpClient(httpClientHandler)
            {
                DefaultRequestVersion = new Version(2, 0)
            };

            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            // var ch = GrpcChannel.ForAddress("http://localhost:9999", new GrpcChannelOptions
            var ch = GrpcChannel.ForAddress("https://localhost:44300", new GrpcChannelOptions
            {
                HttpClient = httpClient,
            });

            // var ch = GrpcChannel.ForAddress("http://127.0.0.1:57509", new GrpcChannelOptions
            // {
            //     Credentials = Grpc.Core.ChannelCredentials.Insecure
            // });

            var client = new PadesSigner.PadesSignerClient(ch);

            var sw = Stopwatch.StartNew();
            var iss = Enumerable.Range(0, 5000);

            for (int i = 0; i < 1; i++)
            {
                System.Console.WriteLine(i);
                var response = await client.RemoteSignStartAsync(request);

                // Assert
                Assert.NotNull(response);
                Assert.NotEmpty(response.DigestAlgorithmOid);
                Assert.NotEmpty(response.TransferDataId);
                Assert.NotEmpty(response.ToSignHash);

                var cert = GetCertificate();
                var signature = SignHash(cert, response.ToSignHash.ToByteArray());

                var completeRequest = new SignatureCompleteRequest
                {
                    Signature = ByteString.CopyFrom(signature),
                    CertificateSummary = true,
                    TransferDataId = response.TransferDataId,
                };

                SignatureCompleteResponse completeResponse = null;
                async Task Act()
                {
                    completeResponse = await client.RemoteSignCompleteAsync(completeRequest);
                }

                var exception = await Record.ExceptionAsync(Act);

                // Assert
                Assert.Null(exception);

                Assert.NotNull(completeRequest);
                Assert.NotNull(completeResponse.SignedFileId);
                Assert.NotNull(completeResponse.CertificateSummary);
            }
            return;

            await iss.ParallelForEachAsync(async (i) =>
                {
                    // System.Console.WriteLine(i);
                    // Act
                    var response = await client.RemoteSignStartAsync(request);

                    // Assert
                    Assert.NotNull(response);
                    Assert.NotEmpty(response.DigestAlgorithmOid);
                    Assert.NotEmpty(response.TransferDataId);
                    Assert.NotEmpty(response.ToSignHash);

                    var cert = GetCertificate();
                    var signature = SignHash(cert, response.ToSignHash.ToByteArray());

                    var completeRequest = new SignatureCompleteRequest
                    {
                        Signature = ByteString.CopyFrom(signature),
                        CertificateSummary = true,
                        TransferDataId = response.TransferDataId,
                    };

                    SignatureCompleteResponse completeResponse = null;
                    async Task Act()
                    {
                        completeResponse = await client.RemoteSignCompleteAsync(completeRequest);
                    }

                    var exception = await Record.ExceptionAsync(Act);

                    // Assert
                    Assert.Null(exception);

                    Assert.NotNull(completeRequest);
                    Assert.NotNull(completeResponse.SignedFileId);
                    Assert.NotNull(completeResponse.CertificateSummary);

                    // Assert.Equal(completeResponse.CertificateSummary.Thumbprint, BitConverter.ToString(pkCertificate.Certificate.ThumbprintSHA256));
                    // Assert.Equal(completeResponse.CertificateSummary.SubjectCommonName, cert.SubjectName.Name);
                    // Assert.Equal(completeResponse.CertificateSummary.SubjectDisplayName, pkCertificate.Certificate.SubjectDisplayName);
                });

            sw.Stop();
            Console.WriteLine($"Elapsed: {sw.Elapsed}");
        }
    }
}
