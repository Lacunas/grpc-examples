﻿// using System;
// using System.Linq;
// using System.Net.Http;
// using System.Security.Cryptography.X509Certificates;
// using System.Threading.Tasks;
// using Google.Protobuf;
// using GrpcExample.Core.Models.Proto.Api;

// namespace console_client
// {
//     public class PythonClient
//     {
//         private readonly HttpClient _httpClient = new HttpClient();


//         public async Task RunGrpc()
//         {
//             const string certPath = "./content/Alan Mathison Turing.pfx";
//             // var pdf = await File.ReadAllBytesAsync("./content/SampleDocument.pdf");
//             var cert = new X509Certificate2(certPath, "1234", X509KeyStorageFlags.MachineKeySet).Export(X509ContentType.Cert);

//             var startRequest = new SignatureStartRequest
//             {
//                 Certificate = ByteString.CopyFrom(cert),
//             };

//             var iss = Enumerable.Range(0, 2000);
//             await iss.ForEachAsync(async (i) =>
//             {
//                 Console.WriteLine(i);

//                 var response = await _client.PostAsJsonAsyncz(_endpointStart, startRequest);
//                 response.EnsureSuccessStatusCode();

//                 var startResponse = await response.Content.ReadAsJsonAsync<SignatureStartResponse>();

//                 var pkCertificate = await Utils.CertWithKey();
//                 // var signature = pkCertificate.SignHash(DigestAlgorithm.GetInstanceByOid(startResponse.DigestAlgorithmOid), startResponse.ToSignHash);
//                 var signature = pkCertificate.SignData(DigestAlgorithm.GetInstanceByOid(startResponse.DigestAlgorithmOid), startResponse.ToSignBytes);

//                 var completeRequest = new SignatureCompleteRequest
//                 {
//                     Certificate = cert,
//                     Signature = signature,
//                     TransferDataFileId = startResponse.TransferDataFileId
//                 };

//                 response = await _client.PostAsJsonAsyncz(_endpointComplete, completeRequest);
//                 response.EnsureSuccessStatusCode();

//                 var completeResponse = await response.Content.ReadAsJsonAsync<SignatureCompleteResponse>();
//             });
//         }

//         public class RunPkie
//         {

//         }
//     }

// }
