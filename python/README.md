# README

Webpage is exposed at <http://127.0.0.1:8000/static/index.html> on development,
with Swagger on <http://127.0.0.1:8000/docs> for the API.

## Development

### Install dependencies

`poetry install`

### Generate code from protobuf definitions

`poetry run /bin/sh -c 'codegen.sh'`

### Run the application

`PYTHONPATH=$PWD poetry run uvicorn main:app --reload --app-dir ./app`

or

## Using docker

### Create a docker network

```bash
docker network create --driver=bridge --subnet=10.10.10.0/24 grpc-network
```

### Running in development

```bash
docker build --pull --rm -f "development.Dockerfile" -t python-grpc-client:dev .
docker run --rm -it -p 8000:80/tcp --name python-grpc-client-dev python-grpc-client:dev
docker network connect grpc-network python-grpc-client-dev
```

### Running in production

```bash
docker build --pull --rm -f "Dockerfile" -t python-grpc-client:latest "."
docker run -d -p 8000:80 --name python-grpc-client-prod python-grpc-client:latest
docker network connect grpc-network python-grpc-client-prod
```

See <https://www.uvicorn.org/deployment/>

## VSCode

on `.vscode/settings.json` you may want to add:

```json
  "python.autoComplete.extraPaths": [
      "<path-to-current-directory>/grpc-examples/python",
  ],
  "python.pythonPath": "<path-to-poetry-virtualenv>",
```

## Known issues

### /bin/sh: 1: /app/codegen.sh: not found (on docker for windows)

Set `codegen.sh` EOL (End of Line) sequence to **LF**. See:
<https://github.com/postlight/headless-wp-starter/issues/171>
