from pydantic import BaseConfig


class CamelCaseAliasConfig(BaseConfig):
    @staticmethod
    def __to_camel(s: str) -> str:
        from humps import camel
        return camel.case(s)

    BaseConfig.allow_population_by_field_name = True
    BaseConfig.alias_generator = __to_camel


GRPC_TARGET = 'localhost:5000'
