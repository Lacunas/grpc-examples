from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from app.routers import cades_merge, cades_signature

app = FastAPI()
app.mount("/static", StaticFiles(directory="wwwroot"), name="static")
app.include_router(cades_signature.router, prefix="/api/cades")
app.include_router(cades_merge.router, prefix="/api/cades")
