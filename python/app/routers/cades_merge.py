# import grpc
from base64 import b64decode, b64encode
from typing import List, Optional

import grpc.experimental.aio
from fastapi import APIRouter
from pydantic import BaseModel

from ..common import GRPC_TARGET, CamelCaseAliasConfig
from ..lib.cades_merger_pb2 import CadesMergerRequest
from ..lib.cades_merger_pb2_grpc import CadesMergerStub
from ..lib.storage_providers_pb2 import Datafile

router = APIRouter()


class CadesMergeRequest(CamelCaseAliasConfig, BaseModel):
    files: List[str]
    encapsulated_content: Optional[str]


class CadesMergeResponse(CamelCaseAliasConfig, BaseModel):
    merged_signature: str


@router.post("/merge", response_model=CadesMergeResponse)
async def cades_merge(request: CadesMergeRequest):
    files: List[Datafile] = []
    for file in request.files:
        content = Datafile()
        content.raw = b64decode(file)
        files.append(content)

    complete_request = CadesMergerRequest(files=files)

    encapsulated = request.encapsulated_content
    if (encapsulated and (len(encapsulated) > 0)):
        complete_request.encapsulated_content = b64decode(encapsulated)

    async with grpc.experimental.aio.insecure_channel(GRPC_TARGET) as channel:
        service = CadesMergerStub(channel)
        response = await service.Merge(complete_request)

    merged_signature = b64encode(response.merged_signature)
    return CadesMergeResponse(merged_signature=merged_signature)
