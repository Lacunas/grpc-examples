# import grpc
from base64 import b64decode, b64encode
from typing import List, Optional

import grpc.experimental.aio
from fastapi import APIRouter
from google.protobuf.json_format import MessageToDict, ParseDict
from pydantic import BaseModel

from ..common import GRPC_TARGET, CamelCaseAliasConfig
from ..lib.signer_pb2 import (CadesOptions, SignatureCompleteRequest,
                              SignatureStartRequest)
from ..lib.signer_pb2_grpc import CadesSignerStub
from ..lib.storage_providers_pb2 import Datafile

router = APIRouter()


class CadesStartRequest(CamelCaseAliasConfig, BaseModel):
    certificate: str
    content: str
    detached: Optional[bool] = False


class CadesStartResponse(CamelCaseAliasConfig, BaseModel):
    to_sign_hash: str
    token: str
    digest_algorithm_oid: str


class CadesCompleteRequest(CamelCaseAliasConfig, BaseModel):
    signature: str
    token: str


class CadesCompleteResponse(CamelCaseAliasConfig, BaseModel):
    signed_content: str


@router.post("/start", response_model=CadesStartResponse)
async def cades_start(request: CadesStartRequest):
    content = Datafile()
    content.raw = b64decode(request.content)

    certificate = b64decode(request.certificate)

    options = CadesOptions()
    options.detached = request.detached

    start_request = SignatureStartRequest(certificate=certificate,
                                          content=content,
                                          cades_options=options)

    async with grpc.experimental.aio.insecure_channel(GRPC_TARGET) as channel:
        service = CadesSignerStub(channel)
        response = await service.RemoteSignStart(start_request)

    return MessageToDict(response)


@router.post("/complete", response_model=CadesCompleteResponse)
async def cades_complete(request: CadesCompleteRequest):
    complete_request = ParseDict(request.dict(), SignatureCompleteRequest())

    async with grpc.experimental.aio.insecure_channel(GRPC_TARGET) as channel:
        service = CadesSignerStub(channel)
        response = await service.RemoteSignComplete(complete_request)

    signed_content = b64encode(response.signed_content)
    return CadesCompleteResponse(signed_content=signed_content)
