#!/bin/sh

# https://github.com/protocolbuffers/protobuf/issues/1491
# https://github.com/protocolbuffers/protobuf/issues/1491#issuecomment-622997606

SOURCE=$1
TARGET=$2

if [ $# -eq 0 ]; then
    SOURCE="./protos" # source of the proto files
    TARGET="app/lib" # where to generate the python files
fi

mkdir -p $TARGET
python3 -m grpc_tools.protoc --python_out=$TARGET --grpc_python_out=$TARGET -I $SOURCE $SOURCE/*.proto

# https://github.com/protocolbuffers/protobuf/issues/1491#issuecomment-562524136
touch $TARGET/__init__.py
2to3 -wnf import $TARGET/ > /dev/null 2>&1
