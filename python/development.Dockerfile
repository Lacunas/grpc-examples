FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

# Install Poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/opt/poetry python && \
  cd /usr/local/bin && \
  ln -s /opt/poetry/bin/poetry && \
  poetry config virtualenvs.create false

# Copy using poetry.lock* in case it doesn't exist yet
COPY ./pyproject.toml ./poetry.lock* /app/

RUN poetry install --no-interaction --no-ansi --no-root

COPY . /app

ENV GRPC_TARGET=grpc-pki-debug:5000
RUN sed -i "s/^GRPC_TARGET.*$/GRPC_TARGET = '${GRPC_TARGET}'/" /app/app/common.py

ENV CODEGEN_SOURCE=/app/protos \
  CODEGEN_TARGET=/app/app/lib

RUN /app/codegen.sh ${CODEGEN_SOURCE} ${CODEGEN_TARGET}
