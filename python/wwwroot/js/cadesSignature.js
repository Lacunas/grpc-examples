// @ts-nocheck
import initializeWebPki, { listCertificates, readCertificate, signHash } from './webPki.js';

// ===========================================================================
// SET YOUR WEB PKI LICENSE HERE
// ===========================================================================
const WEB_PKI_LICENSE = null;

document.addEventListener("DOMContentLoaded", () => init());
// Initialize form fields and webpki when the page is complete loaded.
const init = async () => {
  // Configure action handlers.
  document.getElementById('refreshButton').onclick = _refreshCertificates;
  document.getElementById('signButton').onclick = _sign;
  document.getElementById('filePicker').onchange = _onFileChosen;

  // Inicialize Web PKI and list certificates.
  $.blockUI({ message: 'Initializing ...' });
  await initializeWebPki(WEB_PKI_LICENSE);
  await listCertificates();
  $.unblockUI();
};

async function _refreshCertificates() {
  $.blockUI({ message: 'Refreshing ...' });
  await listCertificates();
  $.unblockUI();
}

function _isDetached() {
  const checkbox = document.getElementById('detachedCheckbox');
  return checkbox.checked === true;
}

async function _sign() {
  $.blockUI({ message: 'Signing ...' });

  // Retrieve the chosen certificate.
  const certThumb = document.getElementById('certificateSelect').value;
  const certContent = await readCertificate(certThumb);
  const file = _getChosenFile();

  const startRequest = {
    'content': await _getFileContentBase64(file),
    'certificate': certContent,
    'detached': _isDetached(),
  };

  // Call server to inicialize the signature.
  let res = await fetch('/api/cades/start', {
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(startRequest),
  });

  const response = await res.json();

  const toSignHash = response.toSignHash;
  const digestAlgOid = response.digestAlgorithmOid;

  console.log(response);

  // 2. Sign the hash value, which is returned by initialization, using the
  //    browser component Web PKI.
  const signature = await signHash(certThumb, toSignHash, digestAlgOid);

  // 3. Complete the signature.
  const completeRequest = {
    'signature': signature,
    'token': response.token,
  };

  // Call server to complete the signature.
  res = await fetch('/api/cades/complete', {
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(completeRequest),
  });

  const completeResponse = await res.json();
  const signedContent = atob(completeResponse.signedContent);

  _updateDownloadButton(Uint8Array.from(signedContent, c => c.charCodeAt(0)));

  $.unblockUI();
};

// -----------------------------------------------------------------------------
//                               Helpers
// -----------------------------------------------------------------------------

function _updateDownloadButton(content) {
  const blob = new Blob([content], { type: 'application/pkcs7-signature' });
  const blobURL = URL.createObjectURL(blob);

  const filename = _getChosenFile().name
    .split('.').reverse().pop(); // strip file extension

  document.getElementById('downloadButton').setAttribute('download', filename + '.p7s');
  document.getElementById('downloadButton').setAttribute('href', blobURL);
  document.getElementById('downloadButton').classList.remove('btn-secondary');
  document.getElementById('downloadButton').classList.add('btn-outline-primary');
}

function _getChosenFile() {
  return document.querySelector("input[type=file]").files[0];
}

function _onFileChosen(evt) {
  document.getElementById('fileInfo').innerText = '';

  const file = evt.target.files[0]; // FileList object

  // Show file information
  let fileInfo = `<strong>${escape(file.name)}</strong>`;
  if (file.size != null) {
    fileInfo += ` - ${file.size} bytes`;
  }
  if (file.type != null) {
    fileInfo += `. Type: ${file.type}`;
  }
  if (file.lastModifiedDate != null) {
    fileInfo += `, last modified: ${file.lastModifiedDate.toLocaleDateString()}`;
  }

  document.getElementById('fileInfo').insertAdjacentHTML('beforeend', `<p>${fileInfo}</p>`);
};

// -----------------------------------------------------------------------------
//                               Helpers
// -----------------------------------------------------------------------------
function _getFileContent(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      var content = reader.result;
      var contentUint8Array = new Uint8Array(content);
      resolve(contentUint8Array);
    };

    reader.onerror = reject;

    reader.readAsArrayBuffer(file);
  });
}

function _getFileContentBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      const content = reader.result.toString();
      const contentBase64 = content.substring(content.indexOf(',') + 1);
      resolve(contentBase64);
    };

    reader.onerror = reject;

    reader.readAsDataURL(file);
  });
}
