// @ts-nocheck

// Initialize form fields.
const init = async () => {
  // Configure action handlers.
  document.getElementById('sendButton').onclick = _merge;
  document.getElementById('filePicker').onchange =
    (e) => _onFilesChosen(e, 'fileInfo');

  document.getElementById('filePickerEncapsulated').onchange =
    (e) => _onFilesChosen(e, 'encapsulatedInfo');
};

async function _merge() {
  $.blockUI({ message: 'Merging ...' });

  const files = await Promise.all(
    _getChosenFiles().map(async f => await _getFileContentBase64(f))
  );

  let request = {
    files,
  };

  const encapsulatedContent = _getEncapsulatedContent();
  if (encapsulatedContent) {
    request.encapsulatedContent = await _getFileContentBase64(encapsulatedContent);
  }

  console.log(request);

  const res = await fetch('/api/cades/merge', {
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(request),
  });

  const response = await res.json();

  console.log(response);

  const mergedSignature = atob(response.mergedSignature);
  _updateDownloadButton(Uint8Array.from(mergedSignature, c => c.charCodeAt(0)));

  $.unblockUI();
};

// -----------------------------------------------------------------------------
//                               Helpers
// -----------------------------------------------------------------------------

function _updateDownloadButton(content) {
  // https://stackoverflow.com/a/2117523
  const uuidv4 = () => {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
  };

  const blob = new Blob([content], { type: 'application/pkcs7-signature' });
  const blobURL = URL.createObjectURL(blob);

  const filename = `merged-${uuidv4()}.p7s`;

  document.getElementById('downloadButton').setAttribute('download', filename + '.p7s');
  document.getElementById('downloadButton').setAttribute('href', blobURL);
  document.getElementById('downloadButton').classList.remove('btn-secondary');
  document.getElementById('downloadButton').classList.add('btn-outline-primary');
}

function _getFileList(id) {
  console.log(`input#${id}`);

  const fileList = document.querySelector(`input#${id}`).files;
  return Array.from(fileList);
}

function _onFilesChosen(evt, id) {
  document.getElementById(id).innerText = '';

  // Show file information
  for (const file of evt.target.files) {
    let fileInfo = `<strong>${escape(file.name)}</strong>`;
    if (file.size != null) {
      fileInfo += ` - ${file.size} bytes`;
    }
    if (file.type != null) {
      fileInfo += `. Type: ${file.type}`;
    }
    if (file.lastModifiedDate != null) {
      fileInfo += `, last modified: ${file.lastModifiedDate.toLocaleDateString()}`;
    }

    document.getElementById(id).insertAdjacentHTML('beforeend', `<p>${fileInfo}</p>`);
  }
};

const _getChosenFiles = () => _getFileList('filePicker');
const _getEncapsulatedContent = () => {
  const fileList = _getFileList('filePickerEncapsulated');
  if (fileList) {
    return fileList[0];
  }
};

function _getFileContent(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      var content = reader.result;
      var contentUint8Array = new Uint8Array(content);
      resolve(contentUint8Array);
    };

    reader.onerror = reject;

    reader.readAsArrayBuffer(file);
  });
}

function _getFileContentBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      const content = reader.result.toString();
      const contentBase64 = content.substring(content.indexOf(',') + 1);
      resolve(contentBase64);
    };

    reader.onerror = reject;

    reader.readAsDataURL(file);
  });
}

document.addEventListener("DOMContentLoaded", init);
