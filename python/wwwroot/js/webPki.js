// @ts-nocheck

let _pki = null;

export default initializeWebPki;

export function initializeWebPki(webPkiLicense) {
  return new Promise((resolve, _reject) => {
    _pki = new LacunaWebPKI(webPkiLicense);

    // Call the init() method on the LacunaWebPKI object, passing a callback for
    // when the component is ready to be used and another to be called when an
    // error occurs on any of the subsequent operations. For more information,
    // see:
    // https://docs.lacunasoftware.com/en-us/articles/web-pki/get-started.html#coding-the-first-lines
    // http://webpki.lacunasoftware.com/Help/classes/LacunaWebPKI.html#method_init
    _pki.init({
      ready: () => resolve(),      // As soon as the component is ready, the promised is resolved.
      defaultError: _onWebPkiError // Generic error callback (see function declaration below).
    });
  });
};

// -----------------------------------------------------------------------------
// Function that loads the certificates, either on startup or when the user
// clicks the "Refresh" button. At this point, the UI is already blocked.
// -----------------------------------------------------------------------------
export function listCertificates() {
  return new Promise((resolve, _reject) => {
    // Call the listCertificates() method to list the user's certificates.
    _pki.listCertificates({
      // ID of the <select> element to be populated with the certificates.
      selectId: 'certificateSelect',

      // Function that will be called to get the text that should be displayed
      // for each option.
      selectOptionFormatter: (cert) => {
        var s = cert.subjectName + ' (issued by ' + cert.issuerName + ')';
        if (new Date() > cert.validityEnd) {
          s = '[EXPIRED] ' + s;
        }

        return s;
      }
    }).success(() => resolve());
  });

};

export function readCertificate(certThumbprint) {
  return new Promise((resolve, _reject) => {
    _pki.readCertificate(certThumbprint)
      .success(encodedCert => resolve(encodedCert));
  });
};

export function signHash(certThumbprint, toSignHash, digestAlg) {
  return new Promise((resolve, _reject) => {
    _pki.signHash({
      thumbprint: certThumbprint,
      hash: toSignHash,
      digestAlgorithm: digestAlg
    }).success(signature => resolve(signature));
  });
};

// -----------------------------------------------------------------------------
// Function called if an error occurs on the Web PKI component.
// -----------------------------------------------------------------------------
function _onWebPkiError(message, error, _origin) {
  // Unblock the UI.
  $.unblockUI();

  // Log the error to the browser console (for debugging purposes).
  if (console) {
    console.log('An error has occurred on the signature browser component: ' + message, error);
  }

  // Show the message to the user. You might want to substitute the alert below
  // with a more user-friendly UI component to show the error.
  _addAlert('danger', 'An error has occurred on the signature browser component: ' + message);
};


// Global method to add an alert on "messagePanel".
function _addAlert(type, message) {
  // Limit message size.
  const MESSAGE_LIMIT = 280;
  if (message.length > MESSAGE_LIMIT) {
    message = message.substring(0, MESSAGE_LIMIT);
    message += '...';
  }

  document.getElementById('messagesPanel').insertAdjacentHTML('beforeend',
    `<div class="alert alert-${type} alert-dismissible fade show" role="alert">`
    + `<span>${message}</span>`
    + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
    + '        <span aria-hidden="true" class="fas fa-times"></span>'
    + '    </button>'
    + '</div>');
}
