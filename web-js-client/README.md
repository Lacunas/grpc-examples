# Running

This folder contains a simple front-end application using only JavaScript that shows how to use the
gRPC client. This project only uses Node.js and Express to provide the static files.

## Running the project

------------------

To run the project, you must have [npm](https://www.npmjs.com/) installed. To check, run:

```
npm --version
```

> If you don't have npm installed, [click here](https://www.npmjs.com/get-npm/).

Then, follow the steps below:

1. Download the project or clone the repository
2. Open a command prompt on the folder `JavaScript`:
3. Install dependencies: `npm install`
4. Run application: `npm start`
5. Access the URL [http://localhost:3000](http://localhost:3000)

Nota: Devido à política de CORS de alguns navegadores (como Firefox), talvez não seja possível comunicar com o serviço gRPC.
