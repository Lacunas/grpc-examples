// @ts-nocheck
const { SignatureStartRequest } = require('pb/signature_start_pb');
const { SignatureCompleteRequest } = require('pb/signature_complete_pb');
const { ProvidedOnRequest } = require('pb/storage_providers_pb');

const { PadesSignerPromiseClient } = require('pb/pades_grpc_web_pb');

const { commonInit, pkiSignHashAsync, readCertificateAsync, refreshCertificateList } = require('pki_common/pkiCommon');
const { readFileAsync, readFileAsDataUrlAsync } = require('pki_common/utils');

// Initialize form fields and webpki
document.addEventListener('DOMContentLoaded', () => {
  $.blockUI({ message: 'Initializing ...' });

  const webPKILicense = null;
  commonInit(webPKILicense);

  document.getElementById('filePicker').addEventListener('change', handleFilePicker, false);
  document.getElementById('signButton').addEventListener('click', async () => { await signButton(); }, false);

  document
    .getElementById('refreshButton')
    .addEventListener('click', refreshCertificateList, false);

  $.unblockUI();
});

const handleFilePicker = (evt) => {
  const f = evt.target.files[0]; // FileList object

  const output = [];
  output.push(`<p><strong>${escape(f.name)}</strong>`);

  if (f.size != null) {
    output.push(' - ', f.size, ' bytes');
  }

  if (f.type != null) {
    output.push('. Type: ', f.type);
  }

  if (f.lastModifiedDate != null) {
    output.push(`, last modified: ${f.lastModifiedDate.toLocaleDateString()}`);
  }

  output.push('</p>');

  document.getElementById('fileOutput').innerHTML = `<p>${output.join('')}</p>`;
};

const signButton = async () => {
  $.blockUI({ message: 'Signing ...' });
  try {
    await signFile();
  } catch (error) {
    console.error(error);
  }
  $.unblockUI();
};

const signerClient = new PadesSignerPromiseClient('http://localhost:8080');

const signFile = async () => {
  // Set file to sign
  const fileToSign = new ProvidedOnRequest();
  fileToSign.setRaw(await processFile());

  const startRequest = new SignatureStartRequest();
  startRequest.setContent(fileToSign);

  // Set certificate
  const certThumb = document.getElementById('certificateSelect').value;
  const certContent = await readCertificateAsync(certThumb);
  startRequest.setCertificate(certContent);

  let response = await signerClient.remoteSignStart(startRequest, {});

  const toSignHash = response.getToSignHash_asB64();
  const transferDataId = response.getTransferDataId();
  const digestAlgOid = response.getDigestAlgorithmOid();

  const signature = await pkiSignHashAsync(certThumb, toSignHash, digestAlgOid);

  const completeRequest = new SignatureCompleteRequest();
  completeRequest.setSignature(signature);
  completeRequest.setTransferDataId(transferDataId);

  response = await signerClient.remoteSignComplete(completeRequest, {});
  console.warn(response.getSignedFileId());
};

/*
* gRPC/protobuf accepts either a UINT8Array or a B64-Encoded string on `bytes`
* fields
*/
async function processFile() {
  const file = document.querySelector('input[type=file]').files[0];
  try {
    const buffer = await readFileAsync(file);
    const content_asU8 = new Uint8Array(buffer);
    return content_asU8;
  } catch (err) {
    console.error(err);
    throw err;
  }
}

async function processFile_asB64() {
  const file = document.querySelector('input[type=file]').files[0];
  try {
    const buffer = await readFileAsDataUrlAsync(file);
    const content = buffer.toString();
    const content_asB64 = content.substring(content.indexOf(',') + 1);
    return content_asB64;
  } catch (err) {
    console.error(err);
    throw err;
  }
}
