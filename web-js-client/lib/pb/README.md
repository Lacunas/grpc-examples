# Protogen

Generate code from protobuf definition.

## mode = grpcwebtext

```bash
protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/Api/greet.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/Api/pades.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/Api/signature_start.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/Api/signature_complete.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/certificate_info.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/storage_providers.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/file.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/validation.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./pb/
```

---

## mode = grpcweb (supports only unary calls)

```bash
protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/Api/greet.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcweb:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/Api/pades.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcweb:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/Api/signature_start.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcweb:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/Api/signature_complete.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcweb:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/certificate_info.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcweb:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/storage_providers.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcweb:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/validation.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcweb:./pb/

protoc --proto_path=../../Protos/Api/ --proto_path=../../Protos/ ../../Protos/file.proto --js_out=import_style=commonjs:./pb/ --grpc-web_out=import_style=commonjs,mode=grpcweb:./pb/
```
