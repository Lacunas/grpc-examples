/**
 * @fileoverview gRPC-Web generated client stub for pades
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var signature_start_pb = require('./signature_start_pb.js')

var signature_complete_pb = require('./signature_complete_pb.js')
const proto = {};
proto.pades = require('./pades_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pades.PadesSignerClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.pades.PadesSignerPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.pades.SimpleSignRequest,
 *   !proto.pades.SimpleSignResponse>}
 */
const methodDescriptor_PadesSigner_SimpleSign = new grpc.web.MethodDescriptor(
  '/pades.PadesSigner/SimpleSign',
  grpc.web.MethodType.UNARY,
  proto.pades.SimpleSignRequest,
  proto.pades.SimpleSignResponse,
  /**
   * @param {!proto.pades.SimpleSignRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pades.SimpleSignResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.pades.SimpleSignRequest,
 *   !proto.pades.SimpleSignResponse>}
 */
const methodInfo_PadesSigner_SimpleSign = new grpc.web.AbstractClientBase.MethodInfo(
  proto.pades.SimpleSignResponse,
  /**
   * @param {!proto.pades.SimpleSignRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.pades.SimpleSignResponse.deserializeBinary
);


/**
 * @param {!proto.pades.SimpleSignRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.pades.SimpleSignResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.pades.SimpleSignResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pades.PadesSignerClient.prototype.simpleSign =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pades.PadesSigner/SimpleSign',
      request,
      metadata || {},
      methodDescriptor_PadesSigner_SimpleSign,
      callback);
};


/**
 * @param {!proto.pades.SimpleSignRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.pades.SimpleSignResponse>}
 *     A native promise that resolves to the response
 */
proto.pades.PadesSignerPromiseClient.prototype.simpleSign =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pades.PadesSigner/SimpleSign',
      request,
      metadata || {},
      methodDescriptor_PadesSigner_SimpleSign);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.remote_signature.SignatureStartRequest,
 *   !proto.remote_signature.SignatureStartResponse>}
 */
const methodDescriptor_PadesSigner_RemoteSignStart = new grpc.web.MethodDescriptor(
  '/pades.PadesSigner/RemoteSignStart',
  grpc.web.MethodType.UNARY,
  signature_start_pb.SignatureStartRequest,
  signature_start_pb.SignatureStartResponse,
  /**
   * @param {!proto.remote_signature.SignatureStartRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  signature_start_pb.SignatureStartResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.remote_signature.SignatureStartRequest,
 *   !proto.remote_signature.SignatureStartResponse>}
 */
const methodInfo_PadesSigner_RemoteSignStart = new grpc.web.AbstractClientBase.MethodInfo(
  signature_start_pb.SignatureStartResponse,
  /**
   * @param {!proto.remote_signature.SignatureStartRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  signature_start_pb.SignatureStartResponse.deserializeBinary
);


/**
 * @param {!proto.remote_signature.SignatureStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.remote_signature.SignatureStartResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.remote_signature.SignatureStartResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pades.PadesSignerClient.prototype.remoteSignStart =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pades.PadesSigner/RemoteSignStart',
      request,
      metadata || {},
      methodDescriptor_PadesSigner_RemoteSignStart,
      callback);
};


/**
 * @param {!proto.remote_signature.SignatureStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.remote_signature.SignatureStartResponse>}
 *     A native promise that resolves to the response
 */
proto.pades.PadesSignerPromiseClient.prototype.remoteSignStart =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pades.PadesSigner/RemoteSignStart',
      request,
      metadata || {},
      methodDescriptor_PadesSigner_RemoteSignStart);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.remote_signature.SignatureCompleteRequest,
 *   !proto.remote_signature.SignatureCompleteResponse>}
 */
const methodDescriptor_PadesSigner_RemoteSignComplete = new grpc.web.MethodDescriptor(
  '/pades.PadesSigner/RemoteSignComplete',
  grpc.web.MethodType.UNARY,
  signature_complete_pb.SignatureCompleteRequest,
  signature_complete_pb.SignatureCompleteResponse,
  /**
   * @param {!proto.remote_signature.SignatureCompleteRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  signature_complete_pb.SignatureCompleteResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.remote_signature.SignatureCompleteRequest,
 *   !proto.remote_signature.SignatureCompleteResponse>}
 */
const methodInfo_PadesSigner_RemoteSignComplete = new grpc.web.AbstractClientBase.MethodInfo(
  signature_complete_pb.SignatureCompleteResponse,
  /**
   * @param {!proto.remote_signature.SignatureCompleteRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  signature_complete_pb.SignatureCompleteResponse.deserializeBinary
);


/**
 * @param {!proto.remote_signature.SignatureCompleteRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.remote_signature.SignatureCompleteResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.remote_signature.SignatureCompleteResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.pades.PadesSignerClient.prototype.remoteSignComplete =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/pades.PadesSigner/RemoteSignComplete',
      request,
      metadata || {},
      methodDescriptor_PadesSigner_RemoteSignComplete,
      callback);
};


/**
 * @param {!proto.remote_signature.SignatureCompleteRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.remote_signature.SignatureCompleteResponse>}
 *     A native promise that resolves to the response
 */
proto.pades.PadesSignerPromiseClient.prototype.remoteSignComplete =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/pades.PadesSigner/RemoteSignComplete',
      request,
      metadata || {},
      methodDescriptor_PadesSigner_RemoteSignComplete);
};


module.exports = proto.pades;

