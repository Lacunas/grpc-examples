const { addAlert } = require('./utils');

export let pki = null;

export const commonInit = (_webPkiLicense) => {
  pki = new LacunaWebPKI(_webPkiLicense);

  // Call the init() method on the LacunaWebPKI object, passing a callback for when the
  // component is ready to be used and another to be called when an error occurs on any of
  // the subsequent operations. For more information, see:
  // https://docs.lacunasoftware.com/en-us/articles/web-pki/get-started.html#coding-the-first-lines
  // http://webpki.lacunasoftware.com/Help/classes/LacunaWebPKI.html#method_init
  pki.init({
    ready: loadCertificates, // As soon as the component is ready we'll load the certificates.
    defaultError: onWebPkiError // Generic error callback (see function declaration below).
  });
};

// --------------------------------------------------------------------------------------------
// Function called when the user clicks the "Refresh" button.
// --------------------------------------------------------------------------------------------
export const refreshCertificateList = () => {
  // Block the UI while we load the certificates.
  $.blockUI({ message: 'Refreshing ...' });

  loadCertificates();
};

// --------------------------------------------------------------------------------------------
// Function that loads the certificates, either on startup or when the user clicks the
// "Refresh" button. At this point, the UI is already blocked.
// --------------------------------------------------------------------------------------------
const loadCertificates = () => {
  const id = 'certificateSelect';

  // Call the listCertificates() method to list the user's certificates.
  pki.listCertificates({
    // ID of the <select> element to be populated with the certificates.
    selectId: id,

    // Function that will be called to get the text that should be displayed for each
    // option.
    selectOptionFormatter: (cert) => {
      var s = cert.subjectName + ' (issued by ' + cert.issuerName + ')';
      if (new Date() > cert.validityEnd) {
        s = '[EXPIRED] ' + s;
      }

      return s;
    }
  }).success(() => $.unblockUI());
};

export const readCertificateAsync = async (certThumbprint) => {
  try {
    return await new Promise((resolve, reject) => {
      pki.readCertificate(certThumbprint)
        .success((certEncoded) => {
          resolve(certEncoded);
        })
        .error((err) => reject(err));
    });
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export const pkiSignHashAsync = async (certThumbprint, toSignHash, digestAlg) => {
  const promise = await new Promise((resolve, reject) => {
    pki.signHash({
      thumbprint: certThumbprint,
      hash: toSignHash,
      digestAlgorithm: digestAlg
    }).success((signature) => resolve(signature))
      .error((err) => reject(err));
  });

  return promise;
};

// --------------------------------------------------------------------------------------------
// Function called if an error occurs on the Web PKI component.
// --------------------------------------------------------------------------------------------
const onWebPkiError = (message, error, _origin) => {
  // Unblock the UI.
  $.unblockUI();

  // Log the error to the browser console (for debugging purposes).
  if (console) {
    console.log('An error has occurred on the signature browser component: ' + message, error);
  }

  // Show the message to the user. You might want to substitute the alert below with a more
  // user-friendly UI component to show the error.
  addAlert('danger', 'An error has occurred on the signature browser component: ' + message);

  return {
    init: commonInit
  };
};
