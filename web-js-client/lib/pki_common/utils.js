// Global method to add an alert on "messagePanel".
export function addAlert(type, message) {
  // Limit message size.
  const MESSAGE_LIMIT = 280;
  if (message.length > MESSAGE_LIMIT) {
    message = message.substring(0, MESSAGE_LIMIT);
    message += '...';
  }

  document.getElementById('messagesPanel').insertAdjacentHTML('beforeend',
    `<div class="alert alert-${type} alert-dismissible fade show" role="alert">`
    + `<span>${message}</span>`
    + '    <button type="button" class="close" data-dismiss="alert" aria-label="Close">'
    + '        <span aria-hidden="true" class="fas fa-times"></span>'
    + '    </button>'
    + '</div>');
}

export function readFileAsync(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      resolve(reader.result);
    };

    reader.onerror = reject;

    reader.readAsArrayBuffer(file);
  });
}

export function readFileAsDataUrlAsync(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      resolve(reader.result);
    };

    reader.onerror = reject;

    reader.readAsDataURL(file);
  });
}
